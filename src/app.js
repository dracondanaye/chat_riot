const express = require('express');
const path = require('path');
const config = require('./config.json');
const start = require('./app/index.js');

const app = express();


app.listen(config.port, function(){
    console.log(`Сервер запущен на порте ${config.port}`);
});
