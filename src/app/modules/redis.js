const redis = require("redis");
const client = require("redis").createClient();

class clientFunction {
    constructor () {

        client.on("error", function (err) {
            console.log("Error " + err);
        });
    }
    async set (key, value) {
        client.set(key, value);
    }
    async get (key) {
        return client.get(key);
    }
}

module.exports = new clientFunction();