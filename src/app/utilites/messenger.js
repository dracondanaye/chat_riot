const messenger = require('redis-messenger');

class Messenger {
    async create () {
        messenger.create(6379, "127.0.0.1", {});
        messenger.register('myNewRoom').then((result) => {
            console.log(result);
        });
        messenger.isAlive('myNewRoom',  function(aliveness) {
            console.log(aliveness)
        });
    }

    async isAlive (room) {
        messenger.isAlive(room,  function(aliveness) {
            console.log(aliveness)
        });
    }

    async sendMessage (room, messange) {
        messenger.send(room,  'moshimoshi',  {  foo:  'bar' } );
    }
}

module.exports = new Messenger();